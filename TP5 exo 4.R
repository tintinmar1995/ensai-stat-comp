surimi <- c(41.28, 45.16, 34.75, 40.76, 43.61, 39.05, 41.20, 
            41.02, 41.33, 40.61, 40.49, 41.77, 42.07,
            44.83, 29.12, 45.59, 41.95, 45.78, 42.89, 40.42, 49.31, 
            44.01, 34.87, 38.60, 39.63, 38.52, 38.52,
            43.95, 49.08, 50.52, 43.85, 40.64, 45.86, 41.25, 50.35, 
            45.18, 39.67, 43.89, 43.89, 42.16) 


donnees = surimi
n = 40

alpha = 0.05  
X_barre = mean(donnees)
ec_emp = sd(donnees)

hist(surimi)
qqplot(qnorm(ppoints(40),X_barre, ec_emp), surimi)
qqline(surimi, distribution = function(p){qnorm(p, X_barre,ec_emp)})
shapiro.test(surimi)

B=10

teta <- numeric(B)
for (i in 1:B)){
  x <-  sample.int(surimi, replace = T)
  teta[i] <- mean(x)
}
quantile(teta, c(2.5, 97.5)/100, na.rm = TRUE)


quantile(surimi, c(0.05))
