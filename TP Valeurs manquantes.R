########## TP6 stat compu
rm(list=ls())

library(MASS)

set.seed(0)
X=mvrnorm(1000,c(0,0,0),matrix(c(1,0.5,0,0.5,1,0,0,0,1),nrow=3,ncol=3,byrow=TRUE))

Y=0+1*X[,1]+1*X[,2]+X[,3]

cov(Y,X[,1])
cov(Y,X[,2])
cov(X[,1],X[,2])

lm(Y~X[,1]+X[,2])

for (i in 1:1000) {
  if (X[i,2] < (-1.5)){
    X[i,2] <- NA
  }
}

sum(is.na(X[,2]))/1000

lm(Y~X[,1]+X[,2],na.action = na.omit)

# Question 3 

#X2 est manquante si x2 <- -1.5
# M�canisme Non Missing At Random



X_manque3 <- X[,2]
X_manque3[which(X_manque3 < -1.5)] <- NA
X_manque2

lm(Y[which(!is.na(X_manque3))] ~ X[which(!is.na(X_manque3)),1] + X_manque3[which(!is.na(X_manque3))])
#on peut utiliser un na.ommit pour lui dire de virer les donn�es manquantes

medX2 = median(X_manque3[which(!is.na(X_manque3))])
X_impuMed = X_manque3
X_impuMed[is.na(X_impuMed)]=medX2

lm(Y ~ X[,1] + X_impuMed)

## Question 4

X_manque4 <- X[,2]

for(i in 1:length(X_manque2)){
  if(runif(1,0,1)<0.5){
    X_manque2[i] <- NA
  }
}

lm(Y~X[,1]+X_manque4,na.action = na.omit)

moy = mean(X_manque4[which(!is.na(X_manque4))])
X_impuMoy = X_manque4
X_impuMoy[is.na(X_impuMoy)]= moy

reg = lm(X_manque4 ~ X[,1],na.action = na.omit)

X2_reg =  reg$coefficients[1] +  reg$coefficients[2]* X[,1]
X2_reg

lm(Y~X[,1]+X2_reg)

#Question 5

X_manque5 = X[,2] 
X_manque5[which(X[,1] < -0)] <- NA

reg2 = lm(X_manque5 ~ X[,1],na.action = na.omit)

X2_reg2 =  reg2$coefficients[1] +  reg2$coefficients[2]* X[,1]
X2_reg2

lm(Y~X[,1]+X2_reg2)


## Exercice 2

taille = 100
mu1 = 80
mu0 = 550
pi1 = 0.7
sigmacarre = 22500
n = nummeric(0)
for(i in 1:taille){  
  if(runif(1,0,1)<pi1){
    n1 = rnorm(taille,mu1,sigmacarre)
  }else{
    n0 = rnorm(taille,mu0,sigmacarre)
  }
}
